<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">1291</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">a32cc612-2538-411a-88e8-66c067c08908</dc:identifier>
        <dc:title>中国的经济制度</dc:title>
        <dc:creator opf:file-as="张五常" opf:role="aut">张五常</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (1.44.0) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2009-09-30T16:00:00+00:00</dc:date>
        <dc:description>神州增订版序
回顾平生，在学术研究上我老老实实地走了一段漫长而又艰苦的路。一九六七写好博士论文《佃农理论》，二○○八写好《中国的经济制度》，相距四十一年，二者皆可传世，思想史上没有谁的智力可以在自己的顶峰维持那幺久。上苍对我格外仁慈，给我有得天独厚之感。
也回顾平生，自己认为足以传世的英语文章约九篇之谱。一般朋友举《佃农》为首，科斯选《蜜蜂的神话》，阿尔钦选《座位票价》，而巴泽尔则肯定《价格管制理论》是无与伦比的。我自己呢？选《中国的经济制度》！两年前写好初稿时我那样想，两年后的今天我还是那样想。似浅实深，这里独立成书的《制度》一文牵涉到的话题广泛，细节多，史实长达三十年，合约理论的分析达到了一个前不见古人的层面，而其中好几处要靠机缘巧合，或时来运到，才找到答案。没有任何「缺环」，完整若天衣无缝也。读者要记住，《制度》一文其实写到二○○七，分析的是新《劳动合同法》引进之前的经济奇迹。
学术过于专注有机会发神经，而我的集中力惊人，往往一发难收。我因而喜欢这里那里分心一下，于是搞摄影、练书法、写散文、好收藏，尝试过的投资或生意无数。这些行为惹来非议，而我喜欢到街头巷尾跑的习惯，使一些无聊之辈认为我早就放弃了学术，不是昔日的史提芬·张云云。这些人不知道经济学的实验室是真实的世界，不多到那里观察算不上是科学。至于那些认为我转向研究中国是浪费了天赋的众君子，属坐井观天，既不知天高，也不知地厚。是的，我这一辈在西方拜师学艺的人知道，在国际学术上中国毫不重要，没有半席之位可言。也怪不得，在学问上炎黄子孙没有一家之言，恐怕不止二百年了。今天老人家西望，竟然发觉那里的经济大师不怎么样。不懂中国，对经济的认识出现了一个大缺环，算不上真的懂经济。
《中国的经济制度》这本英、中二语的书，在香港出版过三次。这次攻进神州，应该有点前途。为此我补做了两件事。其一是在正文之前我加进两篇有关的文章，介绍正文的重要性。其二是在正文中我作了些补充。此文早就修改过无数次。科斯要把该文与其它文章一起结集在美国出版，问我还有没有需要修改的地方。我反复重读，找不到有什么地方要改，但有几处不妨多说几句，尤其是在第八节之后加了四段我认为是重要的。就让国内的同学先读这些补充吧。
是为序。
张五常
二○○九年八月一日</dc:description>
        <dc:publisher>中信出版社</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9787508616438</dc:identifier>
        <dc:language>en</dc:language>
        <dc:subject>张五常</dc:subject>
        <dc:subject>经济学</dc:subject>
        <dc:subject>经济</dc:subject>
        <dc:subject>中国的经济制度</dc:subject>
        <dc:subject>中国</dc:subject>
        <dc:subject>中国经济</dc:subject>
        <dc:subject>经济制度</dc:subject>
        <dc:subject>改革三十年</dc:subject>
        <meta content="{&quot;张五常&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="7" name="calibre:rating"/>
        <meta content="2014-08-22T16:03:09+00:00" name="calibre:timestamp"/>
        <meta content="中国的经济制度" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#myshelves" content="{&quot;is_category&quot;: true, &quot;#extra#&quot;: null, &quot;kind&quot;: &quot;field&quot;, &quot;is_custom&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#myshelves&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;text&quot;, &quot;#value#&quot;: null, &quot;category_sort&quot;: &quot;value&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true, &quot;label&quot;: &quot;myshelves&quot;, &quot;display&quot;: {&quot;use_decorations&quot;: 0}, &quot;name&quot;: &quot;My Shelves&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" type="cover" title="Cover"/>
    </guide>
</package>
