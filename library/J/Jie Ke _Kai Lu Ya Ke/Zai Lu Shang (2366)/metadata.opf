<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2366</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">947ca10e-4732-4fe5-b8a8-b37528585dae</dc:identifier>
        <dc:title>在路上</dc:title>
        <dc:creator opf:file-as="杰克·凯鲁亚克" opf:role="aut">杰克·凯鲁亚克</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (1.44.0) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2010-11-16T16:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p style="text-align: center;"&gt;&lt;strong&gt;&lt;font class="Apple-style-span" color="#55AA00"&gt;凯鲁亚克与“垮掉的一代”&lt;/font&gt;&lt;/strong&gt;&lt;/p&gt; &lt;p&gt;　　半个多世纪前的一个4月里，凯鲁亚克用一部打字机和一卷120英尺长的打印纸完成了《在路上》。在这前后20天日子里，杰克的房间里除了打字的声音以外，就只剩下半空中飞扬的情绪和思想。&lt;/p&gt; &lt;p&gt;　　凯鲁亚克几乎一夜成名，他被封为“垮掉派之王”，但酗酒的毛病也愈发严重。名声或许成了凯鲁亚克最大的负累，他在书中说：“在尘世中默默无闻的人要比在天堂上声名显赫自由自在得多，什么是天堂？什么是尘世？全是些虚无缥缈的想象。”自此他经历了声名显赫与纸醉金迷的最后一段日子。&lt;/p&gt; &lt;p&gt;　　凯鲁亚克是一个时代的象征，但并不代表那个时代文学的最高成就。他作品的文学成就远不及他文本的文化学意义和社会学意义。如果说一件真正的艺术品的面世具有任何重大意义的话，那么《在路上》的出版就是一个重要的历史事件。&lt;/p&gt; &lt;p&gt;　　小说主人公为了追求自由个性，与几个年轻男女随便找个借口就上路了，他们沿途搭车或开车，几次横越美国大陆。一路上他们随心所欲，寻欢作乐，高谈东方禅宗，吸大麻，玩女人。走累了就挡道拦车，夜宿村落。书中的人物不停地穿梭于公路与城市之间，每一段行程都有那么多人在路上，孤独的、忧郁的、快乐的、麻木的……纽约、丹佛、旧金山……城市只是符号，是路上歇息片刻的驿站，每当他们抵达一个地点，却发现梦想仍然在远方，于是只有继续前进。&lt;/p&gt; &lt;p&gt;　　在聚众旅行的狂欢中，几乎没有道德底限。在这种混乱、亢奋而筋疲力尽的得过且过的状态背后，背后蕴涵的人生价值拷问远没有它的语言那么轻快。《在路上》里的人物实际“寻求”的是精神领域的超脱。虽然他们一有借口就横越全国来回奔波，沿途寻找刺激，他们真正的旅途却在精神层面。如果说他们似乎逾越了大部分法律和道德的界限，他们的出发点也仅仅是希望在另一侧找到信仰。&lt;/p&gt; &lt;pre style="text-align: center;"&gt;&lt;b&gt;&lt;font class="Apple-style-span" color="#55AA00"&gt;在路上，在中国&lt;/font&gt;&lt;/b&gt;&lt;/pre&gt; &lt;p&gt;　　《在路上》问世这一年，中国人并不知道，几年后作家出版社出版了《在路上》的节译本，由黄雨石、施咸荣合译。这属于“文革”前的“内部参考”书。这些封面或灰或黄而被称为黄皮书、灰皮书的外国作品，通过各种方式被“偷运”出来，在知识青年中流传阅读。&lt;/p&gt; &lt;p&gt;　　1990年华东师大研究生陶跃庆、何晓丽译出《在路上》，1998年文楚安译的《在路上》问世，它们相继在中国文艺青年并不平静的心湖中激起了涟漪。2006年上海译文出版社又推出了由王永年翻译的《在路上》的新译本。当初，凯鲁亚克从东方禅宗中汲取返璞归真的原生态；如今，中国青年则羡慕其叛逆的行径和无所谓的决绝。&lt;/p&gt; &lt;p&gt;　　地下诗歌运动———无论莽汉主义，还是口语派，多少都受过“在路上”和“垮掉的一代”的影响。其中海子的死则更是一种“在路上”上的终极追求。孤僻、不懂交际、无法融入现代社会、爱情路上坎坷等，都只不过是表面上的原因。海子内心真正追求的是什么呢？他呕心沥血经营起自己的诗歌王国，最后又以血肉之躯来祭奠。深层的哲学原因是什么呢？海子是一直“在路上”，在路上，在朝觐诗歌圣殿的路上，走上了一条类似天梯的路，在新的路上继续跋涉。&lt;/p&gt; &lt;p&gt;　　年轻的崔健那时背一把破吉他，裤脚一高一低地吼出：“脚下这地在走，身边那水在流，可你总是笑我，一无所有……”（《一无所有》）他那强悍而沙哑的歌喉喊出了一代人的迷惘，他掷地有声的疑问立刻被丢进了一个易燃的世界。“我要从南走到北，还要从白走到黑”（《假行僧》），这种随性所至和漫无目标的奔走，成为一代人的理想宣言和精神缩影。&lt;/p&gt; &lt;p&gt;　　后来许巍把自己的专辑命名为《在路上……》。《在路上……》的封面用的是许巍早年的照片，那个有阳光的日子，他背向墙壁，凝视远方，游呤诗人般，在时光和音乐的路上漫步。许巍的“在路上”代表过程或者自由，通向被我们称之为理想和爱的远方。&lt;/p&gt; &lt;p&gt;　　先锋小说家们也共同对《在路上》做出模仿。对流浪生活根深蒂固偏爱的马原，就在一个神奇色彩浓郁的路上，用人生最关键的七年行走，并写了寻找彼岸的《零公里处》。余华成名作《十八岁出门远行》中，只截取“远行”路途中极富隐喻的一个断面，浓缩表现了青春之旅的渴羡、期盼和迷惘的复杂心情。苏童也写了一个短篇———《一个朋友在路上》，描述了一个朋友大学毕业后主动要求去了西藏，但他并非是受了什么刺激或出于某种高尚动机，只是理想是“在路上”，而且是永远在路上。&lt;/p&gt; &lt;p&gt;(&lt;font class="Apple-style-span" color="#FF0000"&gt;子乌注：该版本为陶跃庆 / 何晓丽合译的，网上虽然流传甚多，但是往往张冠李戴，冠以王永年版或文楚安版，多方验证后译者应该无误，特此说明。另：有人说该版本为『净版』，这个问题我不清楚，据陶自己说不是，我想他本人应该还是有些发言权的吧。再另：网上流传的版本都是有6部分，可是我看英文版只到part five，一直很奇怪（我只翻阅……），在一次巧合中，我发现，中文版的前两部都是一样的内容，这样就可以和英文版对应上了，不知道最早的电子版是谁制作的……&lt;/font&gt;)&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>漓江出版社</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9787540706364</dc:identifier>
        <dc:language>zho</dc:language>
        <dc:subject>杰克·凯鲁亚克</dc:subject>
        <dc:subject>垮掉的一代</dc:subject>
        <dc:subject>小说</dc:subject>
        <dc:subject>在路上</dc:subject>
        <dc:subject>美国</dc:subject>
        <dc:subject>外国文学</dc:subject>
        <dc:subject>嬉皮士</dc:subject>
        <dc:subject>美国文学</dc:subject>
        <meta content="{&quot;杰克·凯鲁亚克&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="8" name="calibre:rating"/>
        <meta content="2014-08-22T16:27:30+00:00" name="calibre:timestamp"/>
        <meta content="在路上" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#myshelves" content="{&quot;is_category&quot;: true, &quot;#extra#&quot;: null, &quot;kind&quot;: &quot;field&quot;, &quot;is_custom&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#myshelves&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;text&quot;, &quot;#value#&quot;: null, &quot;category_sort&quot;: &quot;value&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true, &quot;label&quot;: &quot;myshelves&quot;, &quot;display&quot;: {&quot;use_decorations&quot;: 0}, &quot;name&quot;: &quot;My Shelves&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" type="cover" title="Cover"/>
    </guide>
</package>
