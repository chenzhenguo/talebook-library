<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">462</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">dac7e698-0aa8-4c9b-802e-36efb8831bd1</dc:identifier>
        <dc:title>芙蓉镇</dc:title>
        <dc:creator opf:file-as="古华" opf:role="aut">古华</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (1.44.0) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2004-04-30T16:00:00+00:00</dc:date>
        <dc:description>《芙蓉镇》内容简介：芙蓉镇坐落在湘、粤、桂三省交界的峡谷平坝里，古来为商旅歇宿、豪杰聚义、兵家必争的关隘要地。有一溪一河两条水路绕着镇子流过，流出镇口里把路远就汇合了，因而三面环水，是个狭长半岛似的地形。从镇里出发，往南过渡口，可下广东；往西去，过石拱桥，是一条通向广西的大路。不晓得是哪朝哪代，镇守这里的山官大人施行仁政，或者说是附庸风雅图个县志州史留名，命人傍着绿豆色的一溪一河，栽下了几长溜花枝招展、绿荫拂岸的木芙蓉，成为一镇的风水；又派民夫把后山脚下的大片沼泽开掘成方方湖塘，遍种水芙蓉，养鱼，采莲，产藕，作为山官衙门的“官产”。每当湖塘水芙蓉竞开，或是河岸上木芙蓉斗艳的季节，这五岭山脉腹地的平坝，便颇是个花柳繁华之地、温柔富贵之乡了。木芙蓉根、茎、花、皮，均可入药。水芙蓉则上结莲子，下产莲藕，就连它翠绿色的铜锣一样圆圆盖满湖面的肥大叶片，也可让蜻蜒立足，青蛙翘首，露珠儿滴溜；采摘下来，还可给远行的脚夫包中伙饭菜，做荷叶麦子把子，盖小商贩的生意担子，遮赶圩女人的竹篮筐，被放牛娃儿当草帽挡日头……一物百用，各各不同。小河、小溪、小镇，因此得名“芙蓉河”、“玉叶溪”、“芙蓉镇”。
美蓉镇街面不大。十几家铺子、几十户住家紧紧夹着一条青石板街。铺子和铺子是那样的挤密，以至一家煮狗肉，满街闻香气；以至谁家娃儿跌跤碰脱牙、打了碗，街坊邻里心中都有数；以至妹娃家的私房话，年轻夫妇的打情骂俏，都常常被隔壁邻居听了去，传为一镇的秘闻趣事、笑料谈资。偶尔某户人家弟兄内讧，夫妻斗殴，整条街道便会骚动起来，人们往来奔走，相告相劝，如同一河受惊的鸭群，半天不得平息。不是逢圩的日子，街两边的住户还会从各自的阁楼上朝街对面的阁楼搭长竹竿，晾晒一应布物：衣衫裤子，裙子被子。山风吹过，但见通街上空“万国旗”纷纷扬扬，红红绿绿，五花八门。再加上悬挂在各家瓦檐下的串串红辣椒，束束金黄色的苞谷种，个个白里泛青的葫芦瓜，形成两条颜色富丽的夹街彩带……人在下边过，鸡在下边啼，猫狗在下边梭窜，别有一种风情，另成一番景象。 
一年四时八节，镇上居民讲人缘，有互赠吃食的习惯。农历三月三做清明花粑子，四月八蒸莳田米粉肉，五月端午包糯米粽子、喝雄黄艾叶酒，六月六谁家院里的梨瓜、菜瓜熟得早，七月七早禾尝新，八月中秋家做土月饼，九月重阳柿果下树，金秋十月娶亲嫁女，腊月初八制“腊八豆”，十二月二十三日送灶王爷上天……构成家家户户吃食果品的原料虽然大同小异，但一经巧媳妇们配上各种作料做将出来，样式家家不同，味道各各有别，最乐意街坊邻居品尝之后夸赞几句，就像在暗中做着民间副食品展览、色香味品比一般。便是平常日子，谁家吃个有眼珠子、脚爪子的荤腥，也一定不忘夹给隔壁娃儿三块两块，由着娃儿高高兴兴地回家去向父母亲炫耀自己碗里的收获。饭后，做娘的必得牵了娃儿过来坐坐，嘴里尽管拉扯说笑些旁的事，那神色却是完完全全的道谢。</dc:description>
        <dc:publisher>人民文学出版社</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9787020044658</dc:identifier>
        <dc:language>en</dc:language>
        <dc:subject>小说</dc:subject>
        <dc:subject>古华</dc:subject>
        <dc:subject>中国文学</dc:subject>
        <dc:subject>芙蓉镇</dc:subject>
        <dc:subject>茅盾文学奖</dc:subject>
        <dc:subject>文革</dc:subject>
        <dc:subject>具有浓烈的时代特色</dc:subject>
        <dc:subject>矛盾文学奖</dc:subject>
        <meta content="{&quot;古华&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="世纪百强" name="calibre:series"/>
        <meta content="68" name="calibre:series_index"/>
        <meta content="8" name="calibre:rating"/>
        <meta content="2014-08-22T15:30:07+00:00" name="calibre:timestamp"/>
        <meta content="芙蓉镇" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#myshelves" content="{&quot;is_category&quot;: true, &quot;#extra#&quot;: null, &quot;kind&quot;: &quot;field&quot;, &quot;is_custom&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#myshelves&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;text&quot;, &quot;#value#&quot;: null, &quot;category_sort&quot;: &quot;value&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true, &quot;label&quot;: &quot;myshelves&quot;, &quot;display&quot;: {&quot;use_decorations&quot;: 0}, &quot;name&quot;: &quot;My Shelves&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" type="cover" title="Cover"/>
    </guide>
</package>
