<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2266</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">a271873b-52b6-4bdd-9315-529d2407825e</dc:identifier>
        <dc:title>背包十年</dc:title>
        <dc:creator opf:file-as="小鹏" opf:role="aut">小鹏</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (1.44.0) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2010-09-30T16:00:00+00:00</dc:date>
        <dc:description>【编辑推荐】
"一段历经十年，终见彩虹的梦想旅程…
一个以旅行为生的“狂徒”日记…
一名网络时代成就的新文艺青年….
一部梦想拥有者的青春读本….

中国版的凯鲁雅克，中国版的《在路上》
 让读者看到自己曾经的梦想
 励志：关于梦想与成长，自由与坚持
 现在人人都喜欢旅行，可不是没钱就是没时间。背包十年的作者在10年前也跟所有人一样既没钱也没时间，可他就是凭着对旅行的热爱对世界的好奇，让他从一个纯业余的背包客变成了如今的职业旅行者。现在他的旅行不仅不用花钱（有各种赞助），还能赚到钱（卖游记和照片），他的10年背包生涯关于梦想、成长与坚持。是一本现代社会励志的心灵鸡汤。你是否也想复制他的成功之路？
 与市面上的国别体游记不同，这本书是编年体结构。不是描述景点，而是描述一种成长，描述一个青年最绚烂的10年光阴，像朝阳一样，照亮每个心中还有梦想的人。
 美国有一个旅行家俱乐部，把世界分成318个国家和地区，但全世界只有9个人走完这全部所有。作者之前的理想就是想成为世界第10人，中国第一人。但现在他对理想进行了微调，就是通过努力，让更多人喜欢旅行热爱旅行。他从三毛、格瓦拉身上获得关于旅行的梦想，他的使命就是把这种梦想在下一代人的身上传递。
【内容介绍】
"全书分三个部分，用100个故事串联起10年旅途
故事后会有点评，描述当时的心路历程
并配有大量在旅途中拍摄的精美图片
第一章：背包行天下（2001-2004）
以第一次自助背包旅行起，以第一本书的出版结。描述自由自在的旅途生活。
第二章：艰难的旅行路（2005-2007）
描述在旅途中的颠沛流离，父母的不支持，人生中的最低谷；在黑暗中摸索一条可以把旅行良性循环的道路，因为没有前人的成功经验可以复制。
第三章：看见自己的彩虹（2007-2010）
在一次破釜沉舟的旅行之后，终于柳暗花明。现在我的生活就是旅行，写字，拍照，再次旅行，而且旅行有各种赞助，不再为钱发愁；也终于获得家人的认可。
【小鹏语录】——摘自《背包十年》
	天堂，不过是灵魂自由的人来来往往。
	他告诉我世俗的眼光不重要，自己内心的声音才重要；他教会我要去细致观察这个世界，每个人都有自己独一无二的视角。谢谢你，文森特•梵高。
	我们要相信，自己在某个领域是块真金，这是我们战胜生活磨难的底牌。
	无论是否有信仰，只要能够日日自持，控制欲望，善良而不贪婪，怎样将都是一种积极的人生态度。
	我们来丽江，就是为了能好好地哭一场或者醉一场。哭过醉过之后，一切都可以重来。
	对我来说，简单与自由同义。大多数人，一直在做加法，积累了很多，想要放下时，发现自己变成了温水中的青蛙，想跳已经没有力气。而我因为没有负担，才能越飞越高，越走越远。
	在妈妈心中一定有一张世界地图。那地图上没有国家，没有城市，只有我走过的每一步路。我也知道，我的每一步都踏着她的担心。
	旅行者和街头艺人之间有着相通的本质。街头艺人粉墨登场，对他们来说，处处是舞台。而旅行者虽然素面朝天，可在我看来，处处都是人生。
	我喜欢和有天赋的人一起旅行，那是在路途之外，另一个多彩多姿的世界。
	风筝能否高飞，梦想能否实现，关键不在于线有多长风有多大或者有多少外力的支持与帮助，而在于我们自己的态度。是否乐观，是否坚定，是否专注。
	如果把英语比作基本武器，把浅尝辄止地突击学习其他语言比作秘密武器，那自助旅行者打通语言关的终极武器就是微笑了。
	相同的地方，相同的人，相同的事情，但是对不同人施加的影响截然不同。这是别人的旅行无法被复制的原因，也是旅行的魅力所在。
	加尔各答修道院里的义工身份多样，有法国来的学生，有加拿大的司机，来自荷兰的银行家。大家朗声言笑。给某个相熟修女起个无伤大雅的外号，抱怨加尔各答出租车宰客的无良，讨论着两周后即将开始的非洲旅行。大家来这里工作，不但没有任何报酬，还要搭上机票、伙食费、住宿等各项开支。但仍义无反顾地来了，因为大家都明确地知道，帮助别人即是帮助自己，爱别人即是爱自己。
	原来，每一个旅行者真的只是一颗孤独行星。原来，孤独是自由的另外一个名字。
	我曾经做过八份各种各样的工作，原来我最喜欢最擅长的却只是旅行，然后把旅行记录，再和朋友分享。如果这算一种职业，我能做得比任何人都要敬业和出色。
	原来凤凰提供了这样一个地方，让人把遗忘的时光重新品尝。
	他用他的舞步，他的歌声，他的极致，他的独行，把自己的传说变成传奇，把传奇变成神话。其实MJ永远都不会走，在爱他的人心中。
	旅行应该是美学建筑学历史学，而绝对不应该是经济学。如果把在巴黎转机就算去过法国，那我绕地球一圈，哪用的了3000美元？
	所谓坚强，不是在灾难面前不哭，而是要笑着面对以后。
	真正的奢华并不仅是指硬件设施的品牌与造价，更是一种精益求精的服务态度。后者虽然看不见，却能让人感受得到。
	摄影应该像写作一样，是对一瞬间所见所感的记录，是对一个人成长的记录，永远不要追求被大多数人认可。当一个人自信力足够强大，强大到可以建立自己的审美体系和价值标准的时候，还有谁会在意别人怎么说怎么评价？而在这样的时候，你的信念你的坚持已足以把别人感动。
To dream，为了在年华老去的时候不鄙视自己。</dc:description>
        <dc:publisher>中信出版社</dc:publisher>
        <dc:identifier opf:scheme="MOBI-ASIN">7aeb7c24-77df-4ab6-abcc-1347a7aae037</dc:identifier>
        <dc:identifier opf:scheme="ISBN">9787508622828</dc:identifier>
        <dc:language>zho</dc:language>
        <dc:subject>旅行</dc:subject>
        <dc:subject>游记</dc:subject>
        <dc:subject>在路上</dc:subject>
        <dc:subject>小鹏</dc:subject>
        <dc:subject>行走</dc:subject>
        <dc:subject>在计划外简单而随性地活着</dc:subject>
        <dc:subject>心灵</dc:subject>
        <dc:subject>流浪</dc:subject>
        <meta content="{&quot;小鹏&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="7" name="calibre:rating"/>
        <meta content="2014-08-22T16:24:46+00:00" name="calibre:timestamp"/>
        <meta content="背包十年" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#myshelves" content="{&quot;is_category&quot;: true, &quot;#extra#&quot;: null, &quot;kind&quot;: &quot;field&quot;, &quot;is_custom&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#myshelves&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;text&quot;, &quot;#value#&quot;: null, &quot;category_sort&quot;: &quot;value&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true, &quot;label&quot;: &quot;myshelves&quot;, &quot;display&quot;: {&quot;use_decorations&quot;: 0}, &quot;name&quot;: &quot;My Shelves&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" type="cover" title="Cover"/>
    </guide>
</package>
