<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">2086</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">2648e91f-c4d3-4bff-93f9-fb1897040fc6</dc:identifier>
        <dc:title>风萧萧</dc:title>
        <dc:creator opf:file-as="美玉" opf:role="aut">美玉</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (1.44.0) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2013-09-30T16:00:00+00:00</dc:date>
        <dc:description>这是一个浪漫而忧伤的英雄故事，一个战火纷飞年代里家族的命运，一个信仰是如何建立的心理历程，一个少爷到无产阶级忠诚战士的过程。
上部：田氏是浙东玉堂名门，田子德是田家二少爷，从小聪明顽皮。9岁那年海匪抢劫田家，田家壮丁与海匪火拼的枪声惊吓了因难产而病重的母亲，从此桂香就有些疯痴。抗战爆发，刚到南京的大伯一家随政府迁去重庆，堂哥德宁随校船至宜昌时沉船溺水而亡；而大哥德明从小得肺痨，因抗战买不到消炎药改吸白面止病，不到一年便去世；母亲桂香被日本人在玉堂扔下两颗炸弹彻底吓疯，青梅竹马的表姐雪梅上吊自尽……田家开始衰败，父亲诰暄卖了祖业200亩橘园，凑份买了商船，还差半年就中学毕业的子德被迫缀学，开始学习经商和航行。航运生意最终在兵荒马乱中几乎以破产结束，
中部：抗战胜利，大伯一家回到南京。原南京卫戍部队71军也从云南回师，经大伯周旋，在军中谋得中尉一职。刚入伍没多久，东北战势已剑弩拨张，残酷的“精锐”训练结束没多久，子德被派往东北。四战四平时被俘，经解放军教育，加入了中国人民解放军。他开始走进这支他原本很陌生的人民军队，从抗拒、防备、感动、了解、同情到建立新理想，他在战火中终于有了崇高的信仰，就是要建立一个新的、公平的、自由的世界。他经历了战锦州、辽沈战役、平津战役、大军南下、渡江战役、衡宝追击战、涠洲岛之战、解放海南岛、抗美援朝一、二、三、四、五次战役，砥坪里、秋季攻势、坑道战，他是真正的英雄，几乎无战不立功。艰苦的抗美援朝战争，很多战友的牺牲，根本没能摧毁他的意志，他坚信胜利，带着他的战友们坚守阵地……
下部：1953年7月，抗美援朝战争胜利，子德终于回到了祖国。但他是躺着回来的，饥寒艰苦的抗美援朝战争让他得了严重的胃病，而当初与他一起入朝的战友，几乎已牺牲殆尽。带着满身的病痛，带着建设新中国的渴望，带着怀念战友的痛苦，他回到了祖国。28岁的炮兵营长，风度翩翩一表人才，他有很多追求者和崇拜者。在沈阳治疗胃病时，他也遇上了情投意合的女医生刘雪。然而最终他的选择是回到离别9年的玉堂，接回包办婚姻的妻子和一对儿女。他是真正的男人。和平岁月里，他和那个时代的共产党人一样，时刻严格要求着自己和家人。他努力为国家军队培养炮兵人才，不顾劳累尽心尽力。然而朝鲜的冻伤最终要去了他的生命，1967年去世，年仅43岁。去世前他靠止痛药坚持了半年多，照常工作，正常视察考核炮兵。
小说通过田子德的一生，勾画了抗战时期到解放后文革初期这段时间里，中国社会如凤凰涅磐般地剧变。这种改变是忧伤的、被迫的、摧毁性的，但同时又带着新理想、梦想甚至幻想。田子德的前21年是世家少爷，后21年是无产阶级战士，共产党员，解放军炮兵高级指战员。这种戏剧性的变化，并不是他刻意地追求，而是那个剧烈动荡的时代给予的，而他个人多少有些茫然地沉浮在这个大悲剧与大喜剧中。当他走进革命队伍后，在这么一支伟大的军队里，他建立了崇高的革命信仰也是必然的。然而没有英雄是天生的，英雄也有普通人的情感和弱点，从他的家庭、成长、爱情、婚姻、革命、入党、到无悔地追随，我们看到那一代人的承担和付出。他们的生命浪漫而忧伤，他们的情感坚定而执着，他们用生命和热血捍卫着信仰。我确信他们是有信仰的！</dc:description>
        <dc:publisher>解放军文艺出版社</dc:publisher>
        <dc:identifier opf:scheme="ISBN">9787503324543</dc:identifier>
        <dc:identifier opf:scheme="MOBI-ASIN">9aa335cf-83d9-4171-ae4f-320f6fcbaf69</dc:identifier>
        <dc:language>eng</dc:language>
        <dc:subject>历史</dc:subject>
        <dc:subject>历史画卷</dc:subject>
        <dc:subject>家族故事背景</dc:subject>
        <meta content="{&quot;美玉&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2014-08-22T16:21:08+00:00" name="calibre:timestamp"/>
        <meta content="风萧萧" name="calibre:title_sort"/>
        <meta name="calibre:user_metadata:#myshelves" content="{&quot;is_category&quot;: true, &quot;#extra#&quot;: null, &quot;kind&quot;: &quot;field&quot;, &quot;is_custom&quot;: true, &quot;is_csp&quot;: false, &quot;colnum&quot;: 1, &quot;column&quot;: &quot;value&quot;, &quot;rec_index&quot;: 22, &quot;search_terms&quot;: [&quot;#myshelves&quot;], &quot;link_column&quot;: &quot;value&quot;, &quot;is_multiple2&quot;: {}, &quot;is_multiple&quot;: null, &quot;datatype&quot;: &quot;text&quot;, &quot;#value#&quot;: null, &quot;category_sort&quot;: &quot;value&quot;, &quot;table&quot;: &quot;custom_column_1&quot;, &quot;is_editable&quot;: true, &quot;label&quot;: &quot;myshelves&quot;, &quot;display&quot;: {&quot;use_decorations&quot;: 0}, &quot;name&quot;: &quot;My Shelves&quot;}"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" type="cover" title="Cover"/>
    </guide>
</package>
